import { binToHex, hash160, utf8ToBin } from '@bitauth/libauth';
import { MerkleTree } from 'merkletreejs';

/*
 * Get nearest rounded down log2 base using only bitwise operations
 */
export const ulog2 = (u: number): number => {
  let s: number;
  let t: number;

  // eslint-disable-next-line
  t = Number(u > 0xffff) << 4; u >>= t;
  // eslint-disable-next-line
  s = Number(u > 0xff) << 3; u >>= s, t |= s;
  // eslint-disable-next-line
  s = Number(u > 0xf) << 2; u >>= s, t |= s;
  // eslint-disable-next-line
  s = Number(u > 0x3) << 1; u >>= s, t |= s;

  return (t | (u >> 1));
};

/*
 * Transform a full proof of last element in the data set into a compact proof
 */
export const ToCompactProof = (fullProof: Array<Uint8Array>, elementIndex: number): Array<Uint8Array> => {
  let index: number = 0;
  while (elementIndex) {
    if (elementIndex & 1) {
      index += 1;
    } else {
      fullProof.splice(index, 1);
    }

    elementIndex >>= 1;
  }

  return fullProof;
};

/*
 * Transform the compact proof of last element in the data set into a full proof.
 * This involves computation of the hashes of duplicated (right hand sided) merkle tree elements
 *   to fill the gaps in the sparse compact proof
 * Optionally this method also returns the root of the merkle tree in question
 */
export const ToFullProof = (leafHash: Uint8Array, compactProof: Array<Uint8Array>, elementIndex: number):
{ fullProof: Array<Uint8Array>, root: Uint8Array } => {
  let hash = leafHash; // previous element hash
  let index: number = 0;
  const result: Array<Uint8Array> = [];
  let size: number = 0;
  while (elementIndex) {
    if (elementIndex & 1) {
      // simply copy from merkle branch and compute this level's hash
      result.push(compactProof[index]);
      hash = hash160(Uint8Array.from([...compactProof[index], ...hash]));
      index += 1;
    } else {
      if (!size) {
        // first element being leaf hash
        result.push(leafHash);
      } else {
        result.push(hash);
      }

      // compute missing hash from duplicated items
      hash = hash160(Uint8Array.from([...hash, ...hash]));
    }

    size += 1;
    elementIndex >>= 1;
  }

  return { fullProof: result, root: hash };
};

/*
 * Get the new compact proof given the old compact proof info - sparse proof, previous element hash and previous element index
 *
 * This method is the key for a zero-knowledge expansion of a data set given its previous state.
 * Contracts can use it to permissionlessly and trustlessly propose the next state of a decentralized dataset.
 */
export const GetNewCompactProof = (prevLeafHash: Uint8Array, prevCompactProof: Array<Uint8Array>, prevIndex: number):
Array<Uint8Array> => {
  const newIndex: number = prevIndex + 1; // aka binaryPath
  let setBits: number = ((newIndex - 1) ^ (newIndex)) & (newIndex);

  let newCompactProof: Array<Uint8Array> = [];
  if (setBits === 1) {
    newCompactProof = [prevLeafHash, ...prevCompactProof];
  } else if (setBits === (1 << ulog2(prevIndex))) {
    newCompactProof = [ComputeMerkleRootFromBranch(prevLeafHash, prevCompactProof, 0xffffffff)];
  } else {
    let level: number = 0;
    while ((setBits & 1) === 0) {
      level += 1;
      setBits >>= 1;
    }

    const lowPart = prevCompactProof.slice(0, level);
    const highPart = prevCompactProof.slice(level);

    const subRoot: Uint8Array = ComputeMerkleRootFromBranch(prevLeafHash, lowPart, 0xffffffff);
    newCompactProof = [subRoot, ...highPart];
  }

  return newCompactProof;
};

/* To verify a merkle proof, pass the hash of the element in "leaf", the merkle proof in "branch", and the zero-based
index specifying where the element was in the array when the merkle proof was created.
*/
export const ComputeMerkleRootFromBranch = (leaf: Uint8Array, merkleBranch: Array<Uint8Array>, nIndex: number): Uint8Array => {
  let hash: Uint8Array = leaf;
  for (const element of merkleBranch) {
    if (nIndex & 1) {
      hash = hash160(Uint8Array.from([...element, ...hash]));
    } else {
      hash = hash160(Uint8Array.from([...hash, ...element]));
    }
    nIndex >>= 1;
  }
  return hash;
};

/*
 * Serialize a merkle proof into a string compatible with calling getMerkleRoot nexcscript macro
 * To reduce script size, our macro implementation uses OP_EXEC to emulate OP_CONVERT for decoding minimally encoded data
 * so we serialize the proof in reversed order, because OP_EXEC returns the items on the stack in reversed order
 * Note, element size is 20 bytes (HASH160), so we use '14' as the prefix for each element
 */
export function SerializeProof(proof: Array<Uint8Array>): string;
export function SerializeProof(proof: Array<{ position: 'left' | 'right'; data: Buffer }>): string;
export function SerializeProof(proof: Array<any>): string {
  if (!proof.length) {
    throw Error('Empty proof');
  }

  if (proof[0].data) {
    return proof.slice().reverse().map((element) => `14${binToHex(element.data)}`).join('');
  }

  return proof.slice().reverse().map((element) => `14${binToHex(element)}`).join('');
}

export const MerkleHash160 = (value: string | Buffer): Buffer => Buffer.from(hash160(typeof value === 'string' ? utf8ToBin(value as string) : value as any));

/*
 * Utility function to get merkle root and serialized proof from a list of hashes and leaf index, used with `merkleRoot` macro
 */
export const GetMerkleParams = (hashes: Buffer[], leafIndex: number, hash: any = MerkleHash160)
: { root: string, serializedProof: string } => {
  const tree = new MerkleTree(hashes, hash, {
    hashLeaves: false,
    duplicateOdd: true,
  });

  let proof = tree.getProof(hashes[leafIndex]);

  if (proof.length < ulog2(hashes.length - 1) + 1) {
    const index = leafIndex;
    const { fullProof } = ToFullProof(
      Uint8Array.from([...hashes[leafIndex]]),
      proof.map((val) => Uint8Array.from([...val.data])),
      index,
    );
    const positioned = fullProof.map((fullProofElement) => ({
      position: proof.find((val) => val.data.equals(Buffer.from(fullProofElement)))?.position ?? 'left',
      data: Buffer.from(fullProofElement),
    }));

    proof = positioned;
  }

  const root = tree.getRoot().toString('hex');
  const serializedProof = SerializeProof(proof);

  return {
    root,
    serializedProof,
  };
};
