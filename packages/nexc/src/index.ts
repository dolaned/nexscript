export * from './Errors.js';
export * as utils from '@nexscript/utils';
export {
  compileFile, compileString,
} from './compiler.js';

export const version = '1.0.2';
