import { binToHex } from '@bitauth/libauth';
import {
  asmToBytecode,
  asmToScript,
  bytecodeToAsm,
  placeholder,
} from '@nexscript/utils';
import {
  scriptToAddress,
  createInputScript,
  getInputSize,
  getPreimageSize,
  scriptTemplateToAddress,
  scriptTemplateToGroupAddress,
  toGroupAddress,
  toNexaAddress,
} from '../src/utils.js';
import { Network } from '../src/interfaces.js';
import { alicePkh, alicePub } from './fixture/vars.js';

describe('utils', () => {
  describe('getInputSize', () => {
    it('should calculate input size for small script', () => {
      const inputScript = new Uint8Array(100).fill(0);

      const size = getInputSize(inputScript);

      const expectedSize = 100 + 45 + 1;
      expect(size).toEqual(expectedSize);
    });

    it('should calculate input size for large script', () => {
      const inputScript = new Uint8Array(255).fill(0);

      const size = getInputSize(inputScript);

      const expectedSize = 255 + 45 + 3;
      expect(size).toEqual(expectedSize);
    });
  });

  describe('getPreimageSize', () => {
    it('should calculate preimage size for small script', () => {
      const inputScript = new Uint8Array(100).fill(0);

      const size = getPreimageSize(inputScript);

      const expectedSize = 100 + 156 + 1;
      expect(size).toEqual(expectedSize);
    });

    it('should calculate preimage size for large script', () => {
      const inputScript = new Uint8Array(255).fill(0);

      const size = getPreimageSize(inputScript);

      const expectedSize = 255 + 156 + 3;
      expect(size).toEqual(expectedSize);
    });
  });

  describe('createInputScript', () => {
    it('should create an input script without selector or preimage', () => {
      const asm = `${binToHex(alicePkh)} OP_OVER OP_HASH160 OP_EQUALVERIFY OP_CHECKSIG`;
      const redeemScript = asmToScript(asm);
      const args = [alicePub, placeholder(1)];

      const inputScript = createInputScript(redeemScript, args);

      const expectedInputScriptAsm = `00 ${binToHex(alicePub)} ${binToHex(asmToBytecode(asm))}`;
      expect(bytecodeToAsm(inputScript)).toEqual(expectedInputScriptAsm);
    });

    it('should create an input script with selector and preimage', () => {
      const asm = `${binToHex(alicePkh)} OP_OVER OP_HASH160 OP_EQUALVERIFY OP_CHECKSIG`;
      const redeemScript = asmToScript(asm);
      const args = [alicePub, placeholder(1)];
      const selector = 1;
      const preimage = placeholder(1);

      const inputScript = createInputScript(redeemScript, args, selector, preimage);

      const expectedInputScriptAsm = `00 ${binToHex(alicePub)} 00 OP_1 ${binToHex(asmToBytecode(asm))}`;
      expect(bytecodeToAsm(inputScript)).toEqual(expectedInputScriptAsm);
    });
  });

  describe('scriptToAddress', () => { // meaningless on nexa
    it.skip('should convert a redeem script to a cashaddress', () => {
      const asm = `${binToHex(alicePkh)} OP_OVER OP_HASH160 OP_EQUALVERIFY OP_CHECKSIG`;
      const redeemScript = asmToScript(asm);

      const mainnetAddress = scriptToAddress(redeemScript, Network.MAINNET);
      const testnetAddress = scriptToAddress(redeemScript, Network.TESTNET3);
      const regtestAddress = scriptToAddress(redeemScript, Network.REGTEST);

      const expectedMainnetAddress = 'bitcoincash:pr4wzdh0h9d7fpu890lq8xz0c84cpv3nvyc27hzc7y';
      const expectedTestnetAddress = 'bchtest:pr4wzdh0h9d7fpu890lq8xz0c84cpv3nvyuc6sq0ec';
      const expectedRegtestAddress = 'bchreg:pr4wzdh0h9d7fpu890lq8xz0c84cpv3nvyxyv3ru67';

      expect(mainnetAddress).toEqual(expectedMainnetAddress);
      expect(testnetAddress).toEqual(expectedTestnetAddress);
      expect(regtestAddress).toEqual(expectedRegtestAddress);
    });

    it('should test group addresses', () => {
      const pubkeyTemplateScript = asmToScript('OP_1');
      const templateScript = asmToScript('OP_1 OP_DUP OP_DROP');
      const constraintScript = asmToScript('032e71b1e5649b65e364918fd73fdf3ed310efce759bba8ef6b38c18f1787ca5ce');

      const mainnetAddress = scriptTemplateToAddress(pubkeyTemplateScript, constraintScript, [], Network.MAINNET);

      const expectedMainnetAddress = 'nexa:nqtsq5g5am5mrk7m99j36jax6vredr3v2qge07gsgggjl40k';

      expect(mainnetAddress).toEqual(expectedMainnetAddress);

      const groupId = '57f46c1766dc0087b207acde1b3372e9f90b18c7e67242657344dcd2af660000';
      const amount = 10000000000n;
      const mainnetGroupAddress = scriptTemplateToGroupAddress(groupId, amount, templateScript,
        constraintScript, [], Network.MAINNET);
      const expectedMainnetGroupAddress = 'nexa:npx9ggzh73kpwekuqzrmypavmcdnxuhfly9333lxwfpx2u6ymnf27esqqqyqpeqt2spqqqqqznkgmyme95ayzec44nhp5m5zm3wtd87e5y2wa6d3m0djjegafwndxpuk3ck9qyvhlygqlgs2j6k9';
      expect(mainnetGroupAddress).toEqual(expectedMainnetGroupAddress);

      expect(toGroupAddress(expectedMainnetAddress, '322fde1936ac1f1a0ca21b6e5f1f1580c55953b56385a93c3230d54402ff0000', 500n)).toEqual('nexa:nqazqv30mcvndtqlrgx2yxmwtu03tqx9t9fm2cu94y7ryvx4gsp07qqqqt6qz5g5am5mrk7m99j36jax6vredr3v2qge07gsd7w7z9sa');
      expect(toGroupAddress(expectedMainnetGroupAddress, '322fde1936ac1f1a0ca21b6e5f1f1580c55953b56385a93c3230d54402ff0000', 500n)).toEqual('nexa:npxyugpj9l0pjd4vrudqegsmde0379vqc4v48dtrsk5ncv3s64zq9lcqqqp0gqggqrjqk4qzqqqqq98v3kfhjtf6g9n3tt8wrfhg9hzuk60angg5am5mrk7m99j36jax6vs9wytuun');

      expect(toNexaAddress(expectedMainnetAddress)).toEqual(expectedMainnetAddress);
      expect(toNexaAddress('nexa:nqazqv30mcvndtqlrgx2yxmwtu03tqx9t9fm2cu94y7ryvx4gsp07qqqqt6qz5g5am5mrk7m99j36jax6vredr3v2qge07gsd7w7z9sa')).toEqual(expectedMainnetAddress);
    });
  });
});
