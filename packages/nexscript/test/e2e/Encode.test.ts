import { compileString } from '@nexscript/nexc';
import {
  Contract,
  ElectrumNetworkProvider,
} from '../../src/index.js';
import { fund } from '../fixture/vars.js';

describe('Minimal encoding utils', () => {
  beforeAll(async () => {
  });

  it('encode number', async () => {
    const source = `
    contract encodeNumbers() {
      function test() {
        require(encodeNumber(0) == 0x00);
        require(encodeNumber(1) == 0x51);
        require(encodeNumber(16) == 0x60);
        require(encodeNumber(17) == 0x0111);
        require(encodeNumber(1337) == 0x023905);
      }
    }`;

    const artifact = compileString(source);
    const provider = new ElectrumNetworkProvider();

    const contract = new Contract(artifact, [], { provider });
    await fund(contract.address, 10000);
    await contract.functions.test().to(contract.address, 1000n).withoutChange().send();
  });

  it('encode data', async () => {
    const source = `
    contract encodeData() {
      function test() {
        require(encodeData(bytes("0")) == 0x0130);
        require(encodeData(0x00) == 0x0100);
        require(encodeData(0x01) == 0x0101);
        require(encodeData(0x1337) == 0x021337);

        require(encodeData(0x1337) + encodeNumber(0) == 0x02133700);
        require(encodeData(0x1337) + encodeNumber(1) == 0x02133751);
        require(0x10 + 0x20 == 0x1020);
      }
    }`;

    const artifact = compileString(source);
    const provider = new ElectrumNetworkProvider();

    const contract = new Contract(artifact, [], { provider });
    await fund(contract.address, 10000);
    await contract.functions.test().to(contract.address, 1000n).withoutChange().send();
  });
});
