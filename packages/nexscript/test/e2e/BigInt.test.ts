import {
  Contract,
  ElectrumNetworkProvider,
  FailedRequireError,
  FailedTransactionError,
  Reason,
} from '../../src/index.js';
import { getTxOutputs } from '../test-util.js';
import artifact from '../fixture/bigint.json' assert { type: 'json' };
import { fund } from '../fixture/vars.js';

describe('BigInt', () => {
  let bigintContract: Contract;
  const MAX_INT32 = BigInt('2147483647');
  const MAX_INT64 = BigInt('9223372036854775807');

  beforeAll(async () => {
    const provider = new ElectrumNetworkProvider();
    bigintContract = new Contract(artifact, [], { provider });
    await fund(bigintContract.address, 100000);
    console.log(bigintContract.address);
  });

  describe('proofOfBigInt', () => {
    it('should fail when providing a number that fits within 32 bits', async () => {
      // given
      const to = bigintContract.address;
      const amount = 1000n;

      // when
      const txPromise = bigintContract.functions
        .proofOfBigInt(MAX_INT32, 10n)
        .to(to, amount)
        .send();

      // then
      await expect(txPromise).rejects.toThrow(FailedRequireError);
      await expect(txPromise).rejects.toThrow(Reason.VERIFY);
    });

    it('should fail when providing numbers that overflow 64 bits when multiplied', async () => {
      // given
      const to = bigintContract.address;
      const amount = 1000n;

      // when
      const txPromise = bigintContract.functions
        .proofOfBigInt(MAX_INT64 / 9n, 10n)
        .to(to, amount)
        .send();

      // then
      await expect(txPromise).rejects.toThrow(FailedTransactionError);
      await expect(txPromise).rejects.toThrow(Reason.INVALID_NUMBER_RANGE);
    });

    it('should fail when providing a number that does not fit within 64 bits', async () => {
      // given
      const to = bigintContract.address;
      const amount = 1000n;

      // when
      const txPromise = bigintContract.functions
        .proofOfBigInt(MAX_INT64 + 1n, 10n)
        .to(to, amount)
        .send();

      // then
      await expect(txPromise).rejects.toThrow(FailedTransactionError);
      await expect(txPromise).rejects.toThrow(Reason.SCRIPT_NUMBER_OVERFLOW);
    });

    it('should succeed when providing a number within 32b < x < 64b', async () => {
      // given
      const to = bigintContract.address;
      const amount = 1000n;

      // when
      const tx = await bigintContract.functions
        .proofOfBigInt(MAX_INT32 + 1n, 10n)
        .to(to, amount)
        .send();

      // then
      const txOutputs = getTxOutputs(tx);
      expect(txOutputs).toEqual(expect.arrayContaining([{ to, amount }]));
    });
  });
});
