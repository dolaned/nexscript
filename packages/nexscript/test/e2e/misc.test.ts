import { Contract, ElectrumNetworkProvider } from '../../src/index.js';
import { getTxOutputs } from '../test-util.js';
import artifact from '../fixture/simple_covenant.json' assert { type: 'json' };
import { fund } from '../fixture/vars.js';

describe('Simple Covenant', () => {
  const provider = new ElectrumNetworkProvider();
  let covenant: Contract;

  beforeAll(async () => {
    covenant = new Contract(artifact, [], { provider });

    await fund(covenant.address, 100000);

    console.log(covenant.address);
  });

  describe('send', () => {
    it('should succeed', async () => {
      // given
      const to = covenant.address;
      const amount = 1000n;

      // when
      const tx = await covenant.functions.spend().to(to, amount).send();

      // then
      const txOutputs = getTxOutputs(tx);
      expect(txOutputs).toEqual(expect.arrayContaining([{ to, amount }]));
    });
  });
});
