import { SourceArtifact } from '@nexscript/utils';
import {
  Contract,
  ElectrumNetworkProvider,
} from '../../src/index.js';
import {
  aliceAddress, createGroup, fund, mintFungible, sendAuthority,
} from '../fixture/vars.js';
import { getTxOutputs } from '../test-util.js';

const artifact = {
  contracts: [{
    contracts: [],
    contractName: 'test',
    constructorInputs: [],
    abi: [
      {
        name: 'test',
        inputs: [
          {
            name: 'groupId',
            type: 'bytes32',
          },
        ],
      },
    ],
    bytecode: 'OP_DUP OP_EQUALVERIFY',
  }],
  source: 'contract test() {\n      function test(bytes32 groupId) {\n        require(groupId == groupId);\n      }\n    }',
  compiler: {
    name: 'nexc',
    version: '0.4.0',
  },
  updatedAt: '2023-08-31T09:30:52.643Z',
} as SourceArtifact;

describe('Group token support in contract', () => {
  beforeAll(async () => {
  });
  it('should lock authority in contract and release it', async () => {
    const groupId = await createGroup(aliceAddress);

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });

    // lock authority
    await sendAuthority(contract.address, groupId);

    const authorityUtxo = (await contract.getUtxos()).filter((val) => val.token?.groupId === groupId
      && (val.token?.amount || 0n) < 0n)[0];

    // send some plain nex to cover network fees
    await fund(contract.address, 100000);

    const tx = await contract.functions.test(groupId)
      .to([{
        amount: 546n,
        to: aliceAddress,
        token: {
          amount: authorityUtxo.token!.amount,
          groupId,
        },
      }]).send();

    const txOutputs = getTxOutputs(tx);
    expect(txOutputs).toEqual(expect.arrayContaining([{
      amount: 546n,
      to: aliceAddress,
      token: {
        amount: authorityUtxo.token!.amount,
        groupId,
      },
    }]));
  });

  it('should implicitly mint fungible tokens from contract', async () => {
    const groupId = await createGroup(aliceAddress);

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });

    // lock authority
    await sendAuthority(contract.address, groupId);

    const authorityUtxo = (await contract.getUtxos()).filter((val) => val.token?.groupId === groupId
      && (val.token?.amount || 0n) < 0n)[0];

    // send some plain nex to cover network fees
    await fund(contract.address, 100000);

    const tx = await contract.functions.test(groupId)
      .to([{
        amount: 546n,
        to: aliceAddress,
        token: {
          amount: authorityUtxo.token!.amount,
          groupId,
        },
      }, {
        amount: 546n,
        to: aliceAddress,
        token: {
          amount: 500n,
          groupId,
        },
      },
      ]).send();

    const txOutputs = getTxOutputs(tx);
    expect(txOutputs).toEqual(expect.arrayContaining([{
      amount: 546n,
      to: aliceAddress,
      token: {
        amount: authorityUtxo.token!.amount,
        groupId,
      },
    }]));
    expect(txOutputs).toEqual(expect.arrayContaining([{
      amount: 546n,
      to: aliceAddress,
      token: {
        amount: 500n,
        groupId,
      },
    }]));
  });

  it('should not implicitly burn fungible tokens from contract', async () => {
    const groupId = await createGroup(aliceAddress);

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });

    await mintFungible(contract.address, groupId, 1000n);
    await sendAuthority(contract.address, groupId);

    const authorityUtxo = (await contract.getUtxos()).filter((val) => val.token?.groupId === groupId
      && (val.token?.amount || 0n) < 0n)[0];

    // send some plain nex to cover network fees
    await fund(contract.address, 100000);

    const tx = await contract.functions.test(groupId)
      .to([{
        amount: 546n,
        to: contract.address,
        token: {
          amount: authorityUtxo.token!.amount,
          groupId,
        },
      }, {
        amount: 546n,
        to: aliceAddress,
        token: {
          amount: 500n,
          groupId,
        },
      },
      ]).send();

    const txOutputs = getTxOutputs(tx);
    expect(txOutputs).toEqual(expect.arrayContaining([{
      amount: 546n,
      to: contract.address,
      token: {
        amount: authorityUtxo.token!.amount,
        groupId,
      },
    }]));
    expect(txOutputs).toEqual(expect.arrayContaining([{
      amount: 546n,
      to: aliceAddress,
      token: {
        amount: 500n,
        groupId,
      },
    }]));
    expect(txOutputs).toEqual(expect.arrayContaining([{
      amount: 546n,
      to: contract.address,
      token: {
        amount: 500n,
        groupId,
      },
    }]));
  });

  it('should allow to explicitly burn fungible tokens from contract', async () => {
    const groupId = await createGroup(aliceAddress);

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });

    await mintFungible(contract.address, groupId, 1000n);
    await sendAuthority(contract.address, groupId);

    const authorityUtxo = (await contract.getUtxos()).filter((val) => val.token?.groupId === groupId
      && (val.token?.amount || 0n) < 0n)[0];

    // send some plain nex to cover network fees
    await fund(contract.address, 100000);

    const tx = await contract.functions.test(groupId)
      .to([{
        amount: 546n,
        to: contract.address,
        token: {
          amount: authorityUtxo.token!.amount,
          groupId,
        },
      }, {
        amount: 546n,
        to: aliceAddress,
        token: {
          amount: 500n,
          groupId,
        },
      },
      ]).withoutTokenChange().send();

    const txOutputs = getTxOutputs(tx);
    expect(txOutputs).toEqual(expect.arrayContaining([{
      amount: 546n,
      to: contract.address,
      token: {
        amount: authorityUtxo.token!.amount,
        groupId,
      },
    }]));
    expect(txOutputs).toEqual(expect.arrayContaining([{
      amount: 546n,
      to: aliceAddress,
      token: {
        amount: 500n,
        groupId,
      },
    }]));
    expect(txOutputs).not.toEqual(expect.arrayContaining([{
      amount: 546n,
      to: contract.address,
      token: {
        amount: 500n,
        groupId,
      },
    }]));
  });

  it('should implicitly create nonfungible tokens from contract', async () => {
    const groupId = await createGroup(aliceAddress);

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });

    // lock authority
    await sendAuthority(contract.address, groupId);

    const authorityUtxo = (await contract.getUtxos()).filter((val) => val.token?.groupId === groupId
      && (val.token?.amount || 0n) < 0n)[0];

    // send some plain nex to cover network fees
    await fund(contract.address, 100000);

    const tx = await contract.functions.test(groupId)
      .to([{
        amount: 546n,
        to: aliceAddress,
        token: {
          amount: authorityUtxo.token!.amount,
          groupId,
        },
      }, {
        amount: 546n,
        to: aliceAddress,
        token: {
          amount: 1n,
          groupId: `${groupId}abcd`,
        },
      },
      ]).send();

    const txOutputs = getTxOutputs(tx);
    expect(txOutputs).toEqual(expect.arrayContaining([{
      amount: 546n,
      to: aliceAddress,
      token: {
        amount: authorityUtxo.token!.amount,
        groupId,
      },
    }]));
    expect(txOutputs).toEqual(expect.arrayContaining([{
      amount: 546n,
      to: aliceAddress,
      token: {
        amount: 1n,
        groupId: `${groupId}abcd`,
      },
    }]));
  });

  it('should not allow to implicitly burn auth', async () => {
    const groupId = await createGroup(aliceAddress);

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });

    // lock authority
    await sendAuthority(contract.address, groupId);

    const authorityUtxo = (await contract.getUtxos()).filter((val) => val.token?.groupId === groupId
      && (val.token?.amount || 0n) < 0n)[0];

    // send some plain nex to cover network fees
    await fund(contract.address, 100000);
    const otherUtxo = (await contract.getUtxos()).filter((val) => !val.token && val.satoshis > 10000n)[0];

    const tx = await contract.functions.test(groupId)
      .from(authorityUtxo).from(otherUtxo)
      .to([{
        amount: 546n,
        to: aliceAddress,
      }])
      .send();

    const txOutputs = getTxOutputs(tx);
    expect(txOutputs).toEqual(expect.arrayContaining([{
      amount: 546n,
      to: contract.address,
      token: {
        amount: authorityUtxo.token!.amount,
        groupId,
      },
    }]));
  });

  it('should allow to explicitly burn auth', async () => {
    const groupId = await createGroup(aliceAddress);

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });

    // lock authority
    await sendAuthority(contract.address, groupId);

    const authorityUtxo = (await contract.getUtxos()).filter((val) => val.token?.groupId === groupId
      && (val.token?.amount || 0n) < 0n)[0];
    // send some plain nex to cover network fees
    await fund(contract.address, 100000);
    const otherUtxo = (await contract.getUtxos()).filter((val) => !val.token && val.satoshis > 10000n)[0];

    const tx = await contract.functions.test(groupId)
      .from(authorityUtxo).from(otherUtxo)
      .to([{
        amount: 546n,
        to: aliceAddress,
      }])
      .withoutChange()
      .withoutTokenChange()
      .send();

    const txOutputs = getTxOutputs(tx);
    expect(txOutputs).not.toEqual(expect.arrayContaining([{
      amount: 546n,
      to: contract.address,
      token: {
        amount: authorityUtxo.token!.amount,
        groupId,
      },
    }]));
    expect(txOutputs).not.toEqual(expect.arrayContaining([{
      amount: 546n,
      to: aliceAddress,
      token: {
        amount: authorityUtxo.token!.amount,
        groupId,
      },
    }]));
  });
});
