import { SourceArtifact } from '@nexscript/utils';
import {
  Contract,
  ElectrumNetworkProvider,
} from '../../src/index.js';
import { fund } from '../fixture/vars.js';

describe('Unused visible contract parameters', () => {
  beforeAll(async () => {
  });

  it('should succeed with unused parameters', async () => {
    // given
    const artifact = {
      contracts: [{
        contracts: [],
        contractName: 'test',
        constructorInputs: [
          {
            name: 'usedParam', type: 'int', visible: false, unused: false,
          },
          {
            name: 'unusedParam', type: 'int', visible: true, unused: true,
          },
        ],
        abi: [{ name: 'test', inputs: [] }],
        bytecode: 'OP_FROMALTSTACK OP_FROMALTSTACK OP_DROP OP_DUP OP_NUMEQUALVERIFY',
      }],
      source: 'contract test(int usedParam, int visible unused unusedParam) {\n'
        + '    function test() {\n'
        + '      require(usedParam == usedParam);\n'
        + '    }\n'
        + '}',
      compiler: { name: 'nexc', version: '0.1.2' },
      updatedAt: '2023-08-13T07:44:01.189Z',
    } as SourceArtifact;

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [20n, 1n], { provider });
    await fund(contract.address, 100000);

    // when
    const txPromise = contract.functions
      .test()
      .to(contract.address, 90000n)
      .send();

    // then
    await expect(txPromise).resolves.not.toThrow();
  });
});
