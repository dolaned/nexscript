import { binToHex } from '@bitauth/libauth';
import {
  AbiFunction,
  asmToScript,
  calculateBytesize,
  ContractArtifact,
  ContractCreationParams,
  countOpcodes,
  generateRedeemScript,
  Op,
  PrimitiveType,
  Script,
  scriptToBytecode,
  SourceArtifact,
} from '@nexscript/utils';
import nexcore from 'nexcore-lib';
import { Transaction } from './Transaction.js';
import { Argument, encodeArgument } from './Argument.js';
import {
  ContractOptions, Unlocker, Utxo, isUtxoP2PKT,
} from './interfaces.js';
import NetworkProvider from './network/NetworkProvider.js';
import {
  encodeArguments,
  replaceDependencyArgs,
  scriptTemplateToAddress, scriptTemplateToGroupLockingScript, toGroupAddress,
} from './utils.js';
import { ElectrumNetworkProvider } from './network/index.js';
import SignatureTemplate from './SignatureTemplate.js';

export class Contract {
  name: string;
  address: string;
  bytecode: string;
  bytesize: number;
  opcount: number;
  artifact: ContractArtifact;

  functions: Record<string, ContractFunction>;
  unlock: Record<string, ContractUnlocker>;

  private redeemScript: Script;
  private templateScript: Script;
  private constraintScript: Script;
  private visibleArgs: Script = [];
  private dependencyArgs: { [contractName: string]: ContractCreationParams } = {};
  provider: NetworkProvider;

  /**
   * Create new contract instance
   * @param {SourceArtifact | ContractArtifact} artifact - Artifact to instantiate the contract. See @ref compileString
   * @param {Argument[]} constructorArgs - Contract constraint parameters
   * @param {NetworkProvider?} options.provider - Network provider for network interactions
   * @param {string?} options.contractName - if `artifact` is of type SourceArtifact, the name of contract to be instantiated
   * @param {ContractDependencies?} options.dependencyArgs - dependent contract creation parameters
   */
  constructor(
    artifact: SourceArtifact | ContractArtifact,
    constructorArgs: Argument[],
    private options?: ContractOptions,
  ) {
    this.provider = this.options?.provider ?? new ElectrumNetworkProvider();
    const contractName = options?.contractName;

    const isSourceArtifact = 'source' in artifact && 'contracts' in artifact;

    if (isSourceArtifact) {
      if (!artifact.contracts.length) {
        throw Error('Malformed artifact: empty contract list');
      }

      const contractArtifact = contractName
        ? artifact.contracts.find((contract) => contract.contractName === contractName)
        : artifact.contracts[0];
      if (!contractArtifact) {
        throw Error(`Contract '${contractName}' not found in artifact`);
      }
      this.artifact = contractArtifact;
    } else {
      this.artifact = artifact;
    }

    this.dependencyArgs = replaceDependencyArgs(this.artifact, options?.dependencyArgs);

    const expectedProperties = ['abi', 'bytecode', 'constructorInputs', 'contractName'];
    if (!expectedProperties.every((property) => property in this.artifact)) {
      throw new Error('Invalid or incomplete artifact provided');
    }

    const { encodedArgs, visibleArgs } = encodeArguments(
      constructorArgs,
      this.artifact.constructorInputs,
      this.artifact.contractName,
    );

    this.templateScript = asmToScript(this.artifact.bytecode);
    this.constraintScript = encodedArgs;
    this.visibleArgs = visibleArgs;

    this.redeemScript = generateRedeemScript(
      this.templateScript,
      encodedArgs as Uint8Array[],
    );

    // Populate the functions object with the contract's functions
    // (with a special case for single function, which has no "function selector")
    this.functions = {};
    if (this.artifact.abi.length === 1) {
      const f = this.artifact.abi[0];
      this.functions[f.name] = this.createFunction(f);
    } else {
      this.artifact.abi.forEach((f, i) => {
        this.functions[f.name] = this.createFunction(f, i);
      });
    }

    // Populate the unlock object with the contract's functions
    // (with a special case for single function, which has no "function selector")
    this.unlock = {};
    if (this.artifact.abi.length === 1) {
      const f = this.artifact.abi[0];
      this.unlock[f.name] = this.createUnlocker(f);
    } else {
      this.artifact.abi.forEach((f, i) => {
        this.unlock[f.name] = this.createUnlocker(f, i);
      });
    }

    this.name = this.artifact.contractName;
    this.address = scriptTemplateToAddress(
      this.templateScript,
      this.constraintScript,
      this.visibleArgs,
      this.provider.network,
    );
    this.bytecode = binToHex(scriptToBytecode(this.templateScript));
    this.bytesize = calculateBytesize(this.templateScript);
    this.opcount = countOpcodes(this.templateScript);
  }

  tokenAddress(groupId: string, amount: bigint): string {
    return toGroupAddress(this.address, groupId, amount);
  }

  async getBalance(): Promise<bigint> {
    const utxos = await this.getUtxos();
    return utxos.reduce((acc, utxo) => acc + utxo.satoshis, 0n);
  }

  async getUtxos(): Promise<Utxo[]> {
    return this.provider.getUtxos(this.address);
  }

  private createFunction(abiFunction: AbiFunction, selector?: number): ContractFunction {
    return (...args: Argument[]) => {
      if (abiFunction.inputs.length !== args.length) {
        throw new Error(`Incorrect number of arguments passed to function ${abiFunction.name}`);
      }

      // Encode passed args (this also performs type checking)
      let encodedArgs = args
        .map((arg, i) => encodeArgument(arg, abiFunction.inputs[i].type));

      if (selector !== undefined) {
        encodedArgs = [encodeArgument(BigInt(selector), PrimitiveType.INT), ...encodedArgs];
      }

      const unlocker = this.createUnlocker(abiFunction, selector)(...args);

      return new Transaction(
        this.address,
        this.provider,
        this.redeemScript,
        unlocker,
        abiFunction,
        encodedArgs,
        this.templateScript,
        this.constraintScript,
        this.visibleArgs,
      );
    };
  }

  private createUnlocker(abiFunction: AbiFunction, selector?: number): ContractUnlocker {
    const contract = this;
    return (...args: Argument[]) => ({
      addInput({ transaction, input }) {
        // UTXO's with signature templates are signed using P2PKT
        const txo = {
          txId: input.txid,
          outputIndex: input.vout,
          satoshis: Number(input.satoshis),
          address: input.address,
        } as any;

        if (isUtxoP2PKT(input)) {
          transaction.from(txo);
          return;
        }

        // contract input
        const templateScript = nexcore.Script.fromHex(binToHex(scriptToBytecode(contract.templateScript)));
        const constraintScript = contract.constraintScript.length
          ? nexcore.Script.fromHex(binToHex(scriptToBytecode(contract.constraintScript))) : nexcore.Opcode.OP_0;
        const visibleArgs = binToHex(scriptToBytecode(contract.visibleArgs));

        if (input.token) {
          txo.script = scriptTemplateToGroupLockingScript(input.token.groupId, input.token.amount,
            contract.templateScript, contract.constraintScript, contract.visibleArgs);
        }

        const encodedTokenAmount = input.token?.amount ? nexcore.GroupToken.getAmountBuffer(input.token!.amount) : undefined;

        const unspentOutput = new nexcore.Transaction.UnspentOutput(txo);
        const nexcoreInput = new nexcore.Transaction.Input.ScriptTemplate({
          groupId: input.token?.groupId,
          groupAmount: encodedTokenAmount,
          type: nexcore.Transaction.Input.DEFAULT_TYPE,
          output: new nexcore.Transaction.Output({
            script: unspentOutput.script,
            satoshis: unspentOutput.satoshis,
          }),
          prevTxId: unspentOutput.txId,
          outputIndex: unspentOutput.outputIndex,
          script: nexcore.Script.empty(),
          amount: unspentOutput.satoshis,
          templateScript,
          constraintScript,
          visibleArgs,
        }, templateScript, constraintScript, visibleArgs, undefined);
        transaction.addInput(nexcoreInput);
      },

      signInput({ transaction, inputIndex }) {
        if (abiFunction.inputs.length !== args.length) {
          throw new Error(`Incorrect number of arguments passed to function ${abiFunction.name}`);
        }

        // Encode passed args (this also performs type checking)
        let encodedArgs = args
          .map((arg, i) => encodeArgument(arg, abiFunction.inputs[i].type));

        if (selector !== undefined) {
          encodedArgs = [encodeArgument(BigInt(selector), PrimitiveType.INT), ...encodedArgs];
        }

        const completeArgs = encodedArgs.slice().reverse().map((arg) => {
          if (!(arg instanceof SignatureTemplate)) return arg;

          const { privateKey } = arg as SignatureTemplate;
          const nexPrivateKey = nexcore.PrivateKey(binToHex(privateKey), contract.provider.network);

          const signature = nexcore.Transaction.sighash.sign(transaction, nexPrivateKey,
            nexcore.crypto.Signature.SIGHASH_NEXA_ALL, inputIndex, transaction.inputs[inputIndex].templateScript);

          return Uint8Array.from([...signature.toDER()]);
        });

        const satisfierScript = completeArgs.length
          ? nexcore.Script.fromHex(binToHex(scriptToBytecode(completeArgs))) : nexcore.Script.empty();

        transaction.inputs[inputIndex].setScript(nexcore.Script.buildScriptTemplateIn(
          transaction.inputs[inputIndex].templateScript,
          transaction.inputs[inputIndex].constraintScript?.chunks?.length
            ? transaction.inputs[inputIndex].constraintScript : Op.OP_0,
          satisfierScript.toBuffer(),
        ));
      },
    });
  }
}

export type ContractFunction = (...args: Argument[]) => Transaction;
export type ContractUnlocker = (...args: Argument[]) => Unlocker;
