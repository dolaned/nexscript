# NexScript

[![NPM Version](https://img.shields.io/npm/v/@nexscript/nexscript)](https://www.npmjs.com/package/@nexscript/nexscript)
[![NPM Monthly Downloads](https://img.shields.io/npm/dm/@nexscript/nexscript)](https://www.npmjs.com/package/@nexscript/nexscript)
[![NPM License](https://img.shields.io/npm/l/@nexscript/nexscript)](https://www.npmjs.com/package/@nexscript/nexscript)

NexScript is a high-level programming language for smart contracts on Nexa. It offers a strong abstraction layer over Nexa native virtual machine, Nexa Script. Its syntax is based on Ethereum's smart contract language Solidity, but its functionality is very different since smart contracts on Nexa differ greatly from smart contracts on Ethereum. For a detailed comparison of them, refer to the blog post [*Smart Contracts on Ethereum, Bitcoin and Bitcoin Cash*](https://kalis.me/smart-contracts-eth-btc-bch/).

This repository contains the code for the NexScript compiler & command line tool under [`packages/nexc/`](/packages/nexc). This repository also contains the code for the NexScript JavaScript SDK under [`packages/nexscript/`](/packages/nexscript). The source code of the [NexScript.org](https://nexscript.org) website is included under [`website/`](/website). Visit the website for a detailed [Documentation](https://nexscript.org/docs/) on the NexScript language and SDK.

## The NexScript Language
NexScript is a high-level language that allows you to write Bitcoin Cash smart contracts in a straightforward and familiar way. Its syntax is inspired by Ethereum's Solidity language, but its functionality is different since the underlying systems have very different fundamentals. See the [language documentation](https://nexscript.org/docs/language/) for a full reference of the language.

## The NexScript Compiler
NexScript features a compiler as a standalone command line tool, called `nexc`. It can be installed through npm and used to compile `.nex` files into `.json` artifact files. These artifact files can be imported into the NexScript JavaScript SDK (or other SDKs in the future). The `nexc` NPM package can also be imported inside JavaScript files to compile `.nex` files without using the command line tool.

### Installation
```bash
npm install -g @nexscript/nexc
```

### Usage
```bash
Usage: nexc [options] [source_file]

Options:
  -V, --version        Output the version number.
  -o, --output <path>  Specify a file to output the generated artifact.
  -h, --hex            Compile the contract to hex format rather than a full artifact.
  -A, --asm            Compile the contract to ASM format rather than a full artifact.
  -c, --opcount        Display the number of opcodes in the compiled bytecode.
  -s, --size           Display the size in bytes of the compiled bytecode.
  -?, --help           Display help
```

## The NexScript SDK
The main way to interact with NexScript contracts and integrate them into applications is using the NexScript SDK. This SDK allows you to import `.json` artifact files that were compiled using the `nexc` compiler and convert them to `Contract` objects. These objects are used to create new contract instances. These instances are used to interact with the contracts using the functions that were implemented in the `.nex` file. For more information on the NexScript SDK, refer to the [SDK documentation](https://nexscript.org/docs/sdk/).

### Installation
```bash
npm install @nexscript/nexscript
```

### Usage
```ts
import { Contract, ... } from '@nexscript/nexscript';
```

Using the NexScript SDK, you can import contract artifact files, create new instances of these contracts, and interact with these instances:

```ts
...
  // Import the P2PKH artifact
  import P2PKH from './p2pkh-artifact.json' assert { type: 'json' };

  // Instantiate a network provider for NexScript's network operations
  const provider = new ElectrumNetworkProvider('mainnet');

  // Create a new P2PKH contract with constructor arguments: { pkh: pkh }
  const contract = new Contract(P2PKH, [pkh], provider);

  // Get contract balance & output address + balance
  console.log('contract address:', contract.address);
  console.log('contract balance:', await contract.getBalance());

  // Call the spend function with the owner's signature
  // And use it to send 0. 000 100 00 BCH back to the contract's address
  const txDetails = await contract.functions
    .spend(pk, new SignatureTemplate(keypair))
    .to(contract.address, 10000)
    .send();

  console.log(txDetails);
...
```

## Examples
If you want to see NexScript in action and check out its usage, there are several example contracts in the [`examples/`](/examples) directory. The `.nex` files contain example contracts, and the `.ts` files contain example usage of the NexScript SDK to interact with these contracts.

The "Hello World" of NexScript contracts is defining the P2PKH pattern inside a contract, which can be found under [`examples/p2pkh.nex`](/examples/p2pkh.nex). Its usage can be found under [`examples/p2pkh.ts`](/examples/p2pkh.ts).

### Running the examples
To run the examples, clone this repository and navigate to the `examples/` directory. Since the examples depend on the SDK, be sure to run `npm install` or `yarn` inside the `examples/` directory, which installs all required packages.

```bash
git clone https://gitlab.com/nexa/otoplo/nexscript.git
cd nexscript/examples
npm install
```

All `.ts` files in the [`examples/`](/examples) directory can then be executed with `ts-node`.

```bash
npm install -g ts-node
ts-node p2pkh.ts
```

All `.js` files can be executed with `node`.

```bash
node p2pkh.js
```

### Testing

Tests suite could be abstracted away from the bear metal where the cloned repo is stored by the means of docker containsers.

[@nexscript/nexascipt](https://www.npmjs.com/package/@nexscript/nexscript) requires the use of `nexad` and `rostrum` both runnig in regtest mode. To facilitate the
execution of such tests a series of docker files and a docker compose configuration have been provided under the `jest/` folder.

To execute [@nexscript/nexascipt](https://www.npmjs.com/package/@nexscript/nexscript) we need to create the docker images that we will use in the docker compose definition (all the following commands assume the relevant docker utils software are installed in the testing machine). We are going to start with the nexad docker image:

```bash
cd jest/docker/nexad
docker buildx build --no-cache --progress plain -t nexa/nexa:v1.4.0.1 --platform linux/amd64 --load .
```

Then move to the rostrum docker image:

```bash
cd ../rostrum
docker buildx build --no-cache --progress plain -t nexa/rostrum:v10.1.0 --platform linux/amd64 --load .
```

Once the above terminate successfully we can move on and start the docker compose service with the following command

```bash
cd ../..
pwd  #this should print out the aboslute path for the jest folder
docker compose up -d
cd ..
pwd #this should print to stdout the absolute root path for your repo
yarn test
# once finished
docker compose down -v
```

Executing `yarn test` from the root path will execute the tests suites for all 3 packages: `nexscript`, `utils` and `nexc`. If you want to run a single tests suite, e.g. `utils`, you need to change path to the sub package of choice and then execute `yarn test` "locally":

```bash
cd packages/utils
yarn test
```

### Deploy new version

Assume for a moment there you have all the needed changes in your code to deploy a new version, eg 1.0.2, then from the repo root path execute

```
yarn run update-version 1.0.2
yarn publish-all
```
