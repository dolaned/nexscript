---
title: Release Notes
---

## v1.0.2

* Use testnet-explorer.nexa.org as a rostrum testnet server
* Fix a typos in the README.md file
* Fix shields.io badges for the @nexscript/nexscript package
* Update rostrum Docker file to use the latest nightly version

## v1.0.1

* Use bitauth 2.1.0
* Update jest docker files to use nexad 1.4.0.1 and rostrum 10.0.0
* Improve docker compose configuration
* Use the correct nexa derivation path in nexscript tests suite

## v1.0.0

* Add new `encodeNumber` and `encodeData` global functions for minimal encoding of numbers and data, best used with `new LockingBytecodeP2ST`
* Add `merkleRoot` macro which computes the Merkle root from a serialized proof, leaf hash and leaf index

## v0.9.0

* Add new advanced `TransactionBuilder` class that allows combining UTXOs from multiple different smart contracts and P2PKT UTXOs in a single transaction.

## v0.8.0

* Add merkle tools: ability to derive a compact proof of a next added element, expand proof, etc.
* Fix MAST contracts at terminal laeves being unspendable

## v0.7.0

* Add support for multiplex contracts (see documentation)
* Minor breaking: changed MCP contract syntax

## v0.6.1

* Fix several issues in documentation
* Update electrum cash version

## v0.6.0

* Implement MAST Contract Paths concept (see documentation)

## v0.5.0

* Add support for group tokens in nexscript SDK

* Bugfix: Fix tx state code generation for non-literal parameters

## v0.4.0

* Bugfix: Fix contract unused parameterss ordering

## v0.3.0

* Add additional introspection operations using OP_PUSH_TX_STATE: tx.groupAmountIn, tx.groupAmountOut, tx.groupCountIn, tx.groupCountOut, tx.groupNthInput, tx.groupNthOutput

## v0.2.0

* Add token introspection emulation: tokenGroupId, tokenSubgroupId, subgroupData, tokenAmount, visibleParameters

## v0.1.3

* Support 'unused' modifier for visible contract parameters

## v0.1.2

* Implement LockingBytecodeP2ST in a general form, supporting empty constraints and visible arguments

## v0.1.1

* Add currency aliases: nexa, mnexa, mex

## v0.1.0
* :tada: Initial release.
