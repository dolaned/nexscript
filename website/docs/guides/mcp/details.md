---
title: Implementation details
sidebar_label: Implementation details
---

## MCP contract template

Satisfier Args in main stack = subscript (SS), MP, A, CD, EFGH, IJKLMNOP, SS_ARGS*, DUMMY

Constraint Args in altstack = C_ARGS

SS_ARGS* being SS_ARG_N, …, SS_ARG_1, FunctionIndex?, C_ARG_N, …, C_ARG_1 (inversed order)

C_ARGS being C_ARG_N, …, C_ARG_1 (inversed order)

Note, that satisfier args contain the same copy of constraint args. And constraint args in the altstack are effectively ignored, not used in the template body

**[Main Script in IDE](https://ide.bitauth.com/import-template/eJzNVm1v2zYQ_iuEsA8tYDsk9WIp2Ao4iVt3cRIvdrYV8xBQfKm0yJRB0sm8ov99R8mKk6bZgn3YJiDmi3jP3XO8e6JPwTeWF3LFgsOgcG5tDw8OSiEHeenYxhUDXq8O_ERqV3Lmylr3nVytK-Zk_xYPWtvBb7bWQS8Q0nJTrv0pgBsh65gWzAhkS_2xkn3FuKsNeoyHOjzkCubQxkqLZmzbd3V_tsmrkvdP5bY_YbZAr2Z0djp53YOTEq1q6xCEtwKILxCbqCQqtUdD3gWc19Ld1eZmsNRLvShKcOLB9t4bx0fHEzTnha6NgaA_auY2RtoeMlJsOJBokGz5B6Aq5AzTFiiBSztAR5Iz786feAzK0KSUhhleQIAVOpFOmlWpSwsBo1eTk9foRm57qHSIVbZGdrNe18ZZdMccL_q1rraIVyXwswNIsmYrCdmdNylF8y7ILjlwwmfCldIGh5-C-k5L4yeP72ZR-PSI8rYUGwjprqgRZ5C4tdQCKVOvgEbpI6gq6R54vWjgekGLBR5-Caqa38DORjeTX3vBLTMly6vWPzD7uve1KW99huBAe_G81s7UlX3GMxQBLNx27RcT4Zef4XkQyqcuBpjtjK70Lrr2EOwcHABvyA6QhurkN0jVZiV3nC2Uj1VwV0v97cXsGr9BL3sA9OTq7OwDmC0DtgxeaucN5_Pr0eW7a9LY5v_IloIt_p3KkOYyIZiRSMQkyxXnGYsTInisQsHCIRsyougbbzkZzSckwb6Kc6hiXqD3359Oz84vZg1WKtMsSmkYpklKOY6HSSZCMiRpGGYY9jjNkyQX-TNY47fvJg2Oimii4oyojOCYqiziUcw5wwSLWJE4D3maUzxUz-AcnzQoJON5DPGIGOhxxYZDHOJIZllIU5wyEVEmU5mkX6JUkinElhrnGJ6Xp3UlzQ201pq5ood0DQVzK431Xd5pyR2Doq2RqWvXRBjTRLzs4gC-rUW00UIaUArregiKjfgfenLpb-C-naCq2_6COu9K24_Q-4u2EzqJ3TdKK2vTp3V_L0p5LbZLDe5OrmbNuLgYTRfzxej4tFnuUvjkFRDlzdMkuqrrNcq3TvJayObs_KdRi3d5sWjGt5cXZ4-QQ2-oIbOGrR5uGOm8LMNcrjZNjPBu_PP4eKl90rrMfGUCJvdH28OwYyAmbZ3ZcNd2eftmH__9GuP9PCVhHklOJIvDjEVZSiOWpkIJzNOQ55SIPFZSyGeKdXTUEtBix6xJxHTarrrf_5qkyBVLMqYSrFKMBYbOHEaEyCENh1JSJZKEkJSQZ0n6nvzXaP4NGcYiFgqKMaEikSmIGxmCTsg4HIJQiUyFlKeKhn9Bxv-1cvX_v7s8w1hFGQm5SgXN01CpREVJGsuYMiJUHIWgc-yJDj6g66l2So--65Su1bEuAQ3d8_cNBxiavn-x68Z4_MPVaPrj-PL92w9f0YFmi5yP340W41aGdgnba02b_wXqXt5L7H5_NzyE3uV6Nh0d74HbyXi2mLR-51dH_tz5dSNCdhcObvdAhzZGd5tPr_SLGw2a74_2c00K_zEE34_XFFN8jWNQ392KPFpRv_KfSe3_lOAQf_4T7VlwaQ==)**

[**Loop script in IDE**](https://ide.bitauth.com/import-template/eJx1U9lu2zAQ_BVC6EMC-JDlOygKyEfgIIeNWklR1EVAUauItUwKJGXXDfLvXVJ2ExeJHsRdYjmzM1w-e580y2BDvQsvM6bQF80mT6ARc0NLkzWY3DRtAMJwRg2Xom5gU-TUQH3rN6qzjV9aCq_mJaCZ4oWtQriQaENFQlVCNBdPOdRTyoxU5BSPHPGIyaghpQZNFnRfN7K-KOOcs_o17OszqjNytggW17PzGlYC2UhtCLa3QYj_EF1XQLiwaMRSYL0As5Nq3ViJlYgyjiQW7JXdEY_GM7JkmZBKYdNPgppSga4RBUnJUIRD0vwPoqbEKCo0SkJK3SAjYNTS2YpTUEpmHBRVLMMGczIBA2rDBdfYMDmbTc7JGvY1wg2huZZEl0UhldFkRw3L6lLke8Jyjvp0A00WdAPo7tJZSpbHJo_mYIV1wnDQ3sWzJ3cClA1O7ybKrD0J3_KkxJZ2mSSMonEFiISkSm5QBrcd5DmYN6xzB1fzKixk-OHlkq1xpxQu-FnztlRxGucVPyp7n71QfGsdwoLq4pkURslcf8CMQ4CJ2Rc2mSU2fcHvTSvPxx4wOhy6F4fuqiLcaTZJLmWB4tEiVI4jytYr8dn_3Y5jvzccxn6aQD8NWmkaD3s9SqE_oK1BN4WkTzvtfucLQYxZuJy1er6dghingGVk5EBaQxZ3h51B0oVei6W03_fbfgeGw3Yw8Ac06QQUBtAbfAASroQf-_itxD9LUVnlMWo9yrMr3n9UuXF8Zq9mVaN984H2WCb7lZgvHif3C7dG8_AmWkbh-Nqlgfvfzif2qWB0N4-uLl20_BZWJ6Z3E7vl4nEYufUgx8XhOLp6mI6-R9PxfDJ9c9aFl1_nt-8wTq4eDgW21nOXW70FSOyk4eN8DPzAf_S7KOuQtU6ywGZ2BkFpN2v-y1_D-qEi)

**SS, MP, A, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || C_ARGS**

OP_DUP = Duplicates the subscript.

**SS, SS, MP, A, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || C_ARGS**

OP_TOALTSTACK = Move the subscript to the altstack ready for execution later.

**SS, MP, A, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || SS, C_ARGS**

OP_HASH160 = Generates the leaf hash of the script.

**H(SS), MP, A, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || SS, C_ARGS**

OP_TOALTSTACK = Move the hash to altstack to prepare op_exec params.

**MP, A, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || H(SS), SS, C_ARGS**

OP_PUSHDATA `<LOOP_BYTECODE>`

**LOOP_BYTECODE, MP, A, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || H(SS), SS, C_ARGS**

OP_SWAP = move LOOP_BYTECODE behind MP.

**MP, LOOP_BYTECODE, A, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || H(SS), SS, C_ARGS**

OP_ROT = rotate a to the top of the stack

**A, MP, LOOP_BYTECODE, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || H(SS), SS, C_ARGS**

OP_FROMALTSTACK = Get H(SS) back from alt stack. we now have proper (inversed) order of arguments for OP_EXEC

**H(SS), A, MP, LOOP_BYTECODE, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || SS, C_ARGS**

OP_3 = Add exec M_Params value of 3.

**3, H(SS), A, MP, LOOP_BYTECODE, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || SS, C_ARGS**

OP_3 = Add exec M_Returns value of 3.

**3, 3, H(SS), A, MP, LOOP_BYTECODE, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || SS, C_ARGS**

OP_EXEC - execute the repeated part, note the produced stack in the loop context

----- Start of repeated section ------

**MP, A, H(SS) ||** 

OP_DUP OP_TOALTSTACK = duplicate the merkle path and stash its copy to altstack

**MP, A, H(SS) || MP**

OP_2 OP_MOD = Gets the remainder of the merkle_path divided by 2.

**0, A, H(SS) || MP**

OP_NOTIF OP_SWAP OP_ENDIF = if remainder is 1, swap A and H(SS), otherwise do nothing

**A, H(SS) || MP**

OP_CAT = Concatenate subscript hash and first Merkle proof hash.

**AH(SS) || MP**

OP_HASH160 = Hash the concatenated subscript hash and the first merkle proof hash.

**AB || MP**

OP_ACTIVEBYTECODE = Get the LOOP_BYTECODE

**LOOP_BYTECODE, AB** **|| MP**

OP_SWAP = swap bytecode with merkle branch

**AB, LOOP_BYTECODE** **|| MP**

OP_FROMALTSTACK = get back merkle path from alt stack

**MP, AB, LOOP_BYTECODE ||**

OP_2 OP_DIV = divide merkle path by two (substitute for OP_RSHIFT, which only works with BIGNUMs)

**MP’, AB, LOOP_BYTECODE ||**

OP_SWAP = swap MP` with AB to prepare the next OP_EXEC argument order (inversed)

**AB, MP’, LOOP_BYTECODE ||**

----- End of repeated section ------

**AB, MP’, LOOP_BYTECODE, CD, EFGH, IJKLMNOP, SS_ARGS, DUMMY || SS**

OP_3 OP_ROLL = move CD to the top of the stack

**CD, AB, MP’, LOOP_BYTECODE, EFGH, IJKLMNOP, SS_ARGS, DUMMY || SS**

OP_3 OP_3 = Add exec M_Params value of 3, Add exec M_Returns value of 3.

**3, 3, CD, AB, MP’, LOOP_BYTECODE, EFGH, IJKLMNOP, SS_ARGS, DUMMY || SS**

// same as above

OP_EXEC

OP_3 OP_ROLL OP_3 OP_3

**3, 3, EFGH, ABCD, MP’, LOOP_BYTECODE, IJKLMNOP, SS_ARGS, DUMMY || SS, C_ARGS**

// same as above

OP_EXEC

OP_3 OP_ROLL OP_3 OP_3

**3, 3, IJKLMNOP, ABCDEFGH, MP’, LOOP_BYTECODE, SS_ARGS, DUMMY || SS, C_ARGS**

// last exec

OP_EXEC

**ABCDEFGHIJKLMNOP, MP’, LOOP_BYTECODE, SS_ARGS, DUMMY || SS, C_ARGS**

OP_NIP OP_NIP = remove MP’ and LOOP_BYTECODE from the stack

**ABCDEFGHIJKLMNOP, SS_ARGS, DUMMY || SS, C_ARGS**

OP_PUSH20 = Push the predefined subscript hash to the stack.

`<0x...>` = ABCDEFGHIJKLMNOP i.e. the Merkle root hash.

**`<0x...>`, ABCDEFGHIJKLMNOP, SS_ARGS, DUMMY || SS, C_ARGS**

OP_EQUALVERIFY = Check if the generated Merkle root matches the defined Merkle root.

**SS_ARGS, DUMMY || SS, C_ARGS**

OP_FROMALTSTACK = Get subscript from altstack.

**SS, SS_ARGS, DUMMY || C_ARGS**

OP_1NEGATE = Push -1 to stack

**`<-1>`**, **SS, SS_ARGS, DUMMY || C_ARGS**

OP_PLACE = place SS at DUMMY

**SS, SS_ARGS, SS || C_ARGS**

OP_DROP = Drop SS from top of the stack

**SS_ARGS, SS || C_ARGS**

OP_DEPTH = get stack depth (N_Params + 1)

**`<N_Params+1>`, SS_ARGS, SS || C_ARGS**

OP_1SUB = get true N_Params value

**`<N_Params>`, SS_ARGS, SS || C_ARGS**

OP_0 = Add exec M_Returns value of 0.

**`<0>`, `<N_Params>`, SS_ARGS, SS || C_ARGS**

OP_EXEC = Execute subscript.

Example

**`<0>`, `<N_Params>`, SS_ARG_3, SS_ARG_2, SS_ARG_1, FunctionIndex, C_ARG_3, C_ARG_2, C_ARG_1, SS || C_ARGS**

if evaluated left to right, they are “pushed” in their appearance order to SS stack

Produced stack in SS:
C_ARG_1, C_ARG_2, C_ARG_3, FunctionIndex, SS_ARG_1, SS_ARG_2, SS_ARG_3

which is correct ordering - (C_ARGS, SS_ARGS needed, low to high)

Because of this pre-formed stack layout, unused arguments should be dropped first.

I.e., this means that a mast contract using “visible unused” constraints should be declared like following:
*contract A(int visible unused a, int b)…*

instead of the required ordering for normal contracts:

*contract A(int a, int visible unused b)…*
